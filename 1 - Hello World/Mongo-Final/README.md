# Subir o exemplo

A ordem que criamos os arquivos importa. Se quisermos referenciar um secret em um arquivo de deployment,
precisamos rodar primeiramente o deployment do secret.


Criando o arquivo de secret
- kubectl apply -f mongo-secret.yml

Verificando se foi criado
- kubectl get secret

Criando o banco mongo 
- kubectl apply -f mongo.yml

Vericando se foi criado
- kubectl get deployment
- kubectl get pod
- kubectl describe pod mongodb-deployment-8f6675bc5-42tf9

As vezes pode demorar, pois puxa a imagem da internet se não tiver no seu computador

Criando o config map
- kubectl apply -f mongo-configmap.yml

Criando o Mongo express
- kubectl apply -f mongo-express.yml 
Verificando se foi criado
- kubectl get pod
- kubectl describe pod  mongo-express-78fcf796b8-rgb44

Subindo o serviço no minikube:
- minikube service mongo-express-service